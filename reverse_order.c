#include <stdio.h>

/* !!! WRONG IMPLEMENTATION, BECAUSE NO RUN-TIME TYPE INFORMATION OF "value" IS KNOWN.
 * WHEN THE MSB OF "value" IS NOT 0x00 IT WORKS CORRECTLY.
 *
 * Reverse endian-ness. Takes as input an integer of the widest possible type.
 * The user SHOULD cast to whatever integer type is the argument passed in.
 * Usage:
 *   unsigned short a = 0xABCD;
 *	 unsigned short r = (unsigned short)dynamic_reverse( a);	
 *
 */
unsigned long long dynamic_reverse( unsigned long long value){
	unsigned long long res = (unsigned char)value;
	while( value >>= 8){    //<-- BUG: this condition is wrong when MSB is 0x00.
		res = res << 8 | (unsigned char)value;
	}
	return res;
}

/* Reverse the endianness of the parameter "value" of whatever integer type.
 * Using unsigned long long (that is, widest possible unsigned) as type for "value" allows to operate on any unsigned type through implicit cast,
 * but then pre-cast type gets lost. The byte size of the pre-cast type must be passed explicitly through "n_bytes". 
 * Usage:
 *		unsigned short s = 0x0012;
 *		unsigned short sr = (unsigned short)reverse( s, sizeof s);
 *
 */
unsigned long long reverse( unsigned long long value, unsigned char n_bytes){
	unsigned long long res = (unsigned char)value;
	switch( n_bytes){
		case 8:
			res = res << 8 | (unsigned char)(value >>= 8); // here and below, masking of higher significant bits of the right-shifted "value" are avoided thanks to cast, that implies no operation at runtime.
			res = res << 8 | (unsigned char)(value >>= 8);
			res = res << 8 | (unsigned char)(value >>= 8);
			res = res << 8 | (unsigned char)(value >>= 8);
		case 4:
			res = res << 8 | (unsigned char)(value >>= 8); 
			res = res << 8 | (unsigned char)(value >>= 8);
		case 2:
			res = res << 8 | (value >>= 8); // no need to mask higher significant bits of the right-shifted "value" here because now they are all 0's.
		case 1:
			return res;
	}
}


/* check reverse() function
 *
 */
void print_reverse_test_result( unsigned long long input, unsigned n_bytes, unsigned long long output, unsigned long long expected){
	if( n_bytes == 8){
		if( output == expected){
			printf("Passed. reverse( 0x%.16llx) = 0x%.16llx.\n", input, output);
		}else{
			printf("Failed! reverse( 0x%.16llx) = 0x%.16llx. 0x%.16llx was expected.\n", input, output, expected);
		}
	}else if( n_bytes == 4){
		if( output == expected){
			printf("Passed. reverse( 0x%.8x) = 0x%.8x.\n", input, output);
		}else{
			printf("Failed! reverse( 0x%.8x) = 0x%.8x. 0x%.8x was expected.\n", input, output, expected);
		}
	}else if( n_bytes == 2){
		if( output == expected){
			printf("Passed. reverse( 0x%.4hx) = 0x%.4hx.\n", input, output);
		}else{
			printf("Failed! reverse( 0x%.4hx) = 0x%.4hx. 0x%.4hx was expected.\n", input, output, expected);
		}
	}else if( n_bytes == 1){
		if( output == expected){
			printf("Passed. reverse( 0x%.2hhx) = 0x%.2hhx.\n", input, output);
		}else{
			printf("Failed! reverse( 0x%.2hhx) = 0x%.2hhx. 0x%.2hhx was expected.\n", input, output, expected);
		}
	}else{
		printf("print_reverse_test_result: wrong argument \"n_bytes\"%d\n", n_bytes);
	}
}

/* check dynamic_reverse() function
 *
 */
void print_dynamic_reverse_test_result( unsigned long long input, unsigned n_bytes, unsigned long long output, unsigned long long expected){
	if( n_bytes == 8){
		if( output == expected){
			printf("Passed. dynamic_reverse( 0x%.16llx) = 0x%.16llx.\n", input, output);
		}else{
			printf("Failed! dynamic_reverse( 0x%.16llx) = 0x%.16llx. 0x%.16llx was expected.\n", input, output, expected);
		}
	}else if( n_bytes == 4){
		if( output == expected){
			printf("Passed. dynamic_reverse( 0x%.8x) = 0x%.8x.\n", input, output);
		}else{
			printf("Failed! dynamic_reverse( 0x%.8x) = 0x%.8x. 0x%.8x was expected.\n", input, output, expected);
		}
	}else if( n_bytes == 2){
		if( output == expected){
			printf("Passed. dynamic_reverse( 0x%.4hx) = 0x%.4hx.\n", input, output);
		}else{
			printf("Failed! dynamic_reverse( 0x%.4hx) = 0x%.4hx. 0x%.4hx was expected.\n", input, output, expected);
		}
	}else if( n_bytes == 1){
		if( output == expected){
			printf("Passed. dynamic_reverse( 0x%.2hhx) = 0x%.2hhx.\n", input, output);
		}else{
			printf("Failed! dynamic_reverse( 0x%.2hhx) = 0x%.2hhx. 0x%.2hhx was expected.\n", input, output, expected);
		}
	}else{
		printf("print_dynamic_reverse_test_result: wrong argument \"n_bytes\"%d\n", n_bytes);
	}
}

/* testing reverse() and dynamic_reverse() functions
 *
 */
int main(){
	printf("Testing reverse_order\n");

	printf("\nReversing 1 byte:\n\n");
	unsigned char c = 0xAB;
	unsigned char cr = dynamic_reverse( c);
	unsigned char c_expected = c;
	print_dynamic_reverse_test_result( c, sizeof c, cr, c_expected);

	cr = reverse( c, sizeof c);
	print_reverse_test_result( c, sizeof c, cr, c_expected);

	printf("\nReversing 2 bytes:\n\n");
	unsigned short s = 0x1234;
	unsigned short sr = dynamic_reverse( s);
	unsigned short s_expected = 0x3412;
	print_dynamic_reverse_test_result( s, sizeof s, sr, s_expected);

	sr = reverse( s, sizeof s);
	print_reverse_test_result( s, sizeof s, sr, s_expected);

	s = 0x0012;
	s_expected = 0x1200;
	sr = dynamic_reverse( s);
	print_dynamic_reverse_test_result( s, sizeof s, sr, s_expected);
	sr = reverse( s, sizeof s);
	print_reverse_test_result( s, sizeof s, sr, s_expected);

	printf("\nReversing 4 bytes:\n\n");
 	unsigned i = 0x12345678;
	unsigned ir = dynamic_reverse( i);
	unsigned i_expected = 0x78563412;
	print_dynamic_reverse_test_result( i, sizeof i, ir, i_expected);

	ir = reverse( i, sizeof( i));
	print_reverse_test_result( i, sizeof i, ir, i_expected);

 	i = 0x00123456;
	ir = dynamic_reverse( i);
	i_expected = 0x56341200;
	print_dynamic_reverse_test_result( i, sizeof i, ir, i_expected);

	ir = reverse( i, sizeof( i));
	print_reverse_test_result( i, sizeof i, ir, i_expected);

	printf("\nReversing 8 bytes:\n\n");
 	unsigned long long ll = 0x1234567890ABCDEF;
	unsigned long long llr = dynamic_reverse( ll);
	unsigned long long ll_expected = 0xEFCDAB9078563412;
	print_dynamic_reverse_test_result( ll, sizeof ll, llr, ll_expected);

	llr = reverse( ll, sizeof( unsigned long long));
	print_reverse_test_result( ll, sizeof ll, llr, ll_expected);

 	ll = 0x001234567890ABCD;
	llr = dynamic_reverse( ll);
	ll_expected = 0xCDAB907856341200;
	print_dynamic_reverse_test_result( ll, sizeof ll, llr, ll_expected);

	llr = reverse( ll, sizeof( unsigned long long));
	print_reverse_test_result( ll, sizeof ll, llr, ll_expected);

}

